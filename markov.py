from re import findall
from random import random

END_PHRASE = "."


class Markov():
    def __init__(self, text=None):
        if text:
            self.text = text
        self.parsedText = []
        self.markov = {}

    def __repr__(self):
        return(f"{str(self.parsedText)}\n{str(self.markov)}")

    def parseText(self, text=None):
        if not text:
            text = self.text
        text = text.lower()
        text = text.split(END_PHRASE)
        for word in text:
            self.parsedText += word.split() + [END_PHRASE]

    def setup(self):
        if not self.parsedText:
            self.parseText()
        previousWord = END_PHRASE
        for word in self.parsedText:
            if not self.markov.get(previousWord):
                self.markov[previousWord] = {}
                self.markov[previousWord]["sum"] = 0
            if self.markov[previousWord].get(word):
                self.markov[previousWord][word] += 1
            else:
                self.markov[previousWord][word] = 1
            self.markov[previousWord]["sum"] += 1
            previousWord = word
        for key in self.markov:
            n_key = self.markov[key].pop("sum")
            for sub_key in self.markov[key]:
                self.markov[key][sub_key] /= n_key

    def generate(self):
        word = END_PHRASE
        phrase = ""
        while word != END_PHRASE or not phrase:
            rand = random()
            probSum = 0
            for nextWord in self.markov[word]:
                probSum += self.markov[word][nextWord]
                if rand <= probSum:
                    word = nextWord
                    phrase += f' {word}' if word != END_PHRASE else END_PHRASE
                    break
        return phrase
